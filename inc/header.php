<!DOCTYPE html>
<html lang="en">

    
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="landing page" />
    <meta name="keywords" content="camelia, responsive" />
    <meta name="author" content="MonnyDesign"/>

    <title>Simona Design</title> <!-- TITLE -->

    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="css/jquery.fancybox.css" type="text/css" media="screen"/>
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <link href="http://fonts.googleapis.com/css?family=Text+Me+One%7CScada:700italic%7CMontserrat:700" rel="stylesheet" type="text/css">
</head>
<body class="grey loading">
    <div id="page-border">
        