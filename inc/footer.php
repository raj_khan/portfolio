 <!-- BEGIN  FOOTER -->
    <nav class="hidden-xs">
        <div class="footer green">
            <!-- BEGIN FOOTER MENU -->
            <div class="footer-menu">
                <a href="index.html">Home</a>
                <a href="about.html">About</a>
                <a href="services.html">Services</a>
                <a href="projects.html">Projects</a>
                <a href="planner.html">Planner</a>
                <a href="contacts.html">Contacts</a>
            </div>
            <!-- END FOOTER MENU -->
        </div>
    </nav>
    <!-- END  FOOTER -->
    <div class="loader-container backdrop">
        <div class="loader">
            <div class="stick"></div>
            <div class="stick"></div>
            <div class="stick"></div>
            <div class="stick"></div>
            <div class="stick"></div>
            <div class="stick"></div>

            <h1>Loading...</h1>
        </div>
    </div>

    <script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/modernizr.custom.53451.js"></script>
    <script type="text/javascript" src="js/raphael-min.js"></script>
    <script type="text/javascript" src="js/unslider.min.js"></script>
    <script type="text/javascript" src="js/jquery.diamonds.js"></script>
    <script type="text/javascript" src="js/main.js"></script>
</body>

<!-- Mirrored from monnydesign.com/md/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 04 Jul 2017 05:05:01 GMT -->
</html>
