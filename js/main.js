/*jshint undef:false */
(function (window, document, $) {
	"use strict";

	window.$ = window.$ = $;

	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
	   $('html').addClass('ismobile');
	}

    /* Function for skill level */
	function initSkills() {

		if ($('.progresscircles').length) {

			$('.progresscircles').find('svg').remove();

			$('.progresscircles').each(function(i) {

				var s = $(this);
				var contWidth = s.width();
				var arc = s.find('.arc');
				arc.attr('id', 'arc'+i);

				var amount = $(arc).data('percent');
				var strkw = $(arc).data('stokewidth');
				var sign = $(arc).data('sign');
				var fontSize = $(arc).data('fontsize');
				var circleColor = $(arc).data('circlecolor');
				var strokeInnerColor = $(arc).data('innerstrokecolor');
				var strokeColor = $(arc).data('strokecolor');
				var textColor = $(arc).data('textcolor');
				var circleSize = $(arc).data('size');
                circleSize = circleSize==='100%' ? Math.min($(this).parent().width(), 180):circleSize;

				if(parseInt(circleSize, 10)+parseInt(strkw, 10)>contWidth) {
					circleSize = contWidth-strkw;
				}

				var fullSize = parseInt(circleSize, 10)+parseInt(strkw, 10);
				var interval;

                //Create raphael object
				var r = Raphael('arc'+i, fullSize, fullSize);

				//draw inner circle
				r.circle(fullSize/2, fullSize/2, circleSize/2).attr({ stroke: strokeInnerColor, "stroke-width": strkw, fill:  circleColor });

				//add text to inner circle
				var title = r.text(fullSize/2, fullSize/2, 0+sign).attr({
					font: fontSize+'px \'Text Me One\', sans-serif',
					fill: textColor
				})
				.toFront();

				r.customAttributes.arc = function (xloc, yloc, value, total, R) {
					var alpha = 360 / total * value,
						a = (90 - alpha) * Math.PI / 180,
						x = xloc + R * Math.cos(a),
						y = yloc - R * Math.sin(a),
						path;
					if (total == value) {
						path = [
							["M", xloc, yloc - R],
							["A", R, R, 0, 1, 1, xloc - 0.01, yloc - R]
						];
					}
					else {
						path = [
							["M", xloc, yloc - R],
							["A", R, R, 0, +(alpha > 180), 1, x, y]
						];
					}
					return {
						path: path
					};
				};

				//make an arc at 150,150 with a radius of 110 that grows from 0 to 40 of 100 with a bounce
				var my_arc = r.path().attr({
					"stroke": strokeColor,
					"stroke-width": strkw,
					arc: [fullSize/2, fullSize/2, 0, 100, circleSize/2]
				});

				var anim = Raphael.animation({
					arc: [fullSize/2, fullSize/2, amount, 100, circleSize/2]
				}, 1500, "easeInOut");

				eve.on("raphael.anim.frame.*", onAnimate);

				function onAnimate() {
					var howMuch = my_arc.attr("arc");
					title.attr("text", Math.floor(howMuch[2]) + sign);
				}

				var animateSkills = false;
				var isIE8 = false;

				//if(animateSkills || isIE8)
				//{
					my_arc.animate(anim.delay(i*200));
				//}

			});

		}

	}


    $.fn.mdFilters = function(filterWrapper, itemsWrapper) {
        "use strict";
        var itemElemetns = $(itemsWrapper).find('.mix');

        $(filterWrapper).children().on('click', function() {
            var filter = $(this).data('filter');
            $(filterWrapper).children().removeClass('active');
            $(this).addClass('active');

            $(itemElemetns).each(function(){
                if(! $(this).hasClass(filter) )
                {
                    $(this).css({
						'opacity': 0.1,
						'-moz-opacity':0.2, /* Old Mozilla */
						'-khtml-opacity': 0.1, /* Safari 1 */
                        'filter': 'alpha(opacity=10)', /* for IE7 */
						'-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=10)', /* for IE8 */

						'-webkit-transform': 'scale(1)',
                        '-moz-transform': 'scale(1)',
						'-ms-transform': 'scale(1)',
                        'transform': 'scale(1)'
                    })
                    .addClass('inactive');
                }
                else {
                     $(this).css({
                        opacity: 1,
                        filter: 'alpha(opacity=100)',
                        '-moz-transform': 'scale(1)',
                        transform: 'scale(1)'
                    })
                    .removeClass('inactive');
                }

                if(filter === 'all') {
					$(itemElemetns).css({
                        opacity: 1,
                        filter: 'alpha(opacity=100)',
                        '-moz-transform': 'scale(1)',
                        transform: 'scale(1)'
                    })
                    .removeClass('inactive');
                }
            });
        });
    };

    $(document).ready(function () {
		"use strict";

        var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/")+1);
        $("#menu-container a").each(function() {
            if($(this).attr("href") == pgurl || $(this).attr("href") == '' ) {
                $(this).addClass("active");
            }
        });

        $('.flip-container').hover(function() {
            if(! $(this).hasClass('hovered')) {
                $(this).addClass('hovered');
			}
        });

        /* Send contact form to email */
        $('#contactform').on('submit', function () {
			var form_data = $(this).serialize() + '&submit=send';
			e.preventDefault();
            $('body').addClass('loading');

            window.setTimeout(function () {
                $.ajax({
                    type : "POST",
                    url : "contacts.php", // Path to contact_form.php
                    data : form_data,
                    dataType : 'jsonp'
                })
                .done(function (data) {
                    $('#callback').html(data.msg).show('slow');
                    if (data.status) {
                        $('#contactform').find('input[type=text], textarea, select').val('');
                    }
                });
                $('body').removeClass('loading');
            }, 1500);
            return false;
        });

        /* Send Planning Form to email */
        $('#planningform').on('submit', function (e) {
			var form_data = $(this).serialize() + '&submit=send';
			e.preventDefault();
            $('body').addClass('loading');

            window.setTimeout(function () {
                $.ajax({
                    type : "POST",
                    url : "planning.php", // Path to contact_form.php
                    data : form_data,
                    dataType : 'jsonp'
                })
                .done(function (data) {
                    $('#callback').html(data.msg).show('slow');
                    if (data.status) {
                        $('#planningform').find('input[type=text], textarea, select').val('');
                    }
                });
                $('body').removeClass('loading');
            }, 1500);
            return false;
        });

		// Initialize projects
		if( $("#filtered-list").length ) {
	        $("#filtered-list").diamonds({
	            size : $(window).width()<500 ? ($(window).width()/2)-30:200,
	            gap :  2,
	            hideIncompleteRow : false, // default: false
	            autoRedraw : true, // default: true
	            itemSelector : ".item"
	        });

			// Initialize project filters
	        $.fn.mdFilters('#drawing-filters', '.diamonds');
		}

        // Initialize stats
        initSkills()

        // Initialize project slider
		if( $('.project-images-place').length ) {
        	$('.project-images-place').unslider();
		}

        $(window).scroll(function() {
            if ( $(this).scrollTop() > 50) {
                $(".navbar.navbar-default").addClass("navbar-fixed-top");
            }
			else {
                $(".navbar.navbar-default").removeClass("navbar-fixed-top");
            }
        });

		$(window).on('resize', function() {
            initSkills();
			if( $("#filtered-list").length ) {
				$("#filtered-list").diamonds('draw');
			}
        });
	});

	$(window).on('load', function() {
		$('.loader-container').fadeOut('slow', function(){
			$('body').removeClass('loading');
		});

		$('body').append(switcherHtml);
		$('.sett').on('click', function() {
			$('#demo-wrapper').toggle('slow');
		});

		/*
		* Color Switcher
		*/
		$('#demo-wrapper li').on('click', function(e) {
			$('body').attr('class', $(this).data('theme'));
		});

		var head  = document.getElementsByTagName('head')[0];
	    var link  = document.createElement('link');
	    link.rel  = 'stylesheet';
	    link.type = 'text/css';
	    link.href = '//monnydesign.com/md/css/switcher.css';
	    link.media = 'all';
	    head.appendChild(link);
	});

	/*
	* Color Switcher
	*/
	var switcherHtml = '<div class="swicther"><div class="sett"><i class="glyphicon glyphicon-cog"></i></div><div id="demo-wrapper"> <h5 class="demo-head">COLORS:</h5> <ul> <li class="color1" data-theme="grey"></li><li class="color2" data-theme="white"></li><li class="color3" data-theme="blue"></li><li class="color4" data-theme="orange"></li></ul> </div></div>';

})(window, document, jQuery);
