
        <!-- BEGIN TOP MENU -->
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand visible-xs" href="#">
                        <img src="img/logo-circle.svg" alt="MonnyDesign">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="navbar-collapse">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="index.html">Home</a></li>
                        <li><a href="about.html">About</a></li>
                        <li><a href="services.html">Services</a></li>
                        <li class="hidden-xs">
                            <a class="navbar-brand" href="#">
                                <img src="img/logo-circle.svg" alt="MonnyDesign">
                            </a>
                        </li>
                        <li><a href="projects.html">Projects</a></li>
                        <li><a href="planner.html">Planner</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- END TOP MENU -->