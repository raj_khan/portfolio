<?php include 'inc/header.php'; ?>
<?php include 'inc/menu.php'; ?>

        <!-- BEGIN PAGE WRAPPER -->
        <div id="wrap" class="container">
            <header class="clearfix">
                <div class="text-center">
                    <h1 class="fewwords">Welcome to <br>MonnyDesign</h1>
                </div>

                <div class="intro-text">
                    <div class="text-center">
                        This is the portfolio of Simona Nikolova.
                        I design interfaces, shape brands and craft things for the web. Take a look at some of the things I"ve worked on.
                    </div>
                </div>
            </header>

            <!-- BEGIN HOME GALLERY -->
            <div class="interactive row">
                <div class="col-xs-12 col-md-10 col-md-offset-1">
                    <div class="flip-wrapper clearfix">
                        <div class="row">
                            <div class="flip-container col-xs-12 col-sm-6 col-md-4">
                                <div class="flipper">
                                    <div class="flipper-front">
                                        <div class="square">
                                            <div class="circle center-circle"></div>
                                        </div>
                                    </div>
                                    <div class="flipper-back">
                                        <a href="projects/logo.html">
                                            <div class="back-img">
                                                <img src="img/6.jpg" alt="Smiley face">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="flip-container col-xs-12 col-sm-6 col-md-4">
                                <div class="flipper">
                                    <div class="flipper-front">
                                        <div class="square">
                                            <div class="circle center-circle"></div>
                                        </div>
                                    </div>
                                    <div class="flipper-back">
                                        <a href="projects/sound.html">
                                            <div class="back-img">
                                                <img src="img/7.jpg" alt="Smiley face">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="flip-container col-xs-12 col-sm-6 col-md-4">
                                <div class="flipper">
                                    <div class="flipper-front">
                                        <div class="square">
                                            <div class="circle center-circle"></div>
                                        </div>
                                    </div>
                                    <div class="flipper-back">
                                        <a href="projects/logo.html">
                                            <div class="back-img">
                                                <img src="img/8.jpg" alt="Smiley face">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="flip-container col-xs-12 col-sm-6 col-md-4">
                                <div class="flipper">
                                    <div class="flipper-front">
                                        <div class="square">
                                            <div class="circle center-circle"></div>
                                        </div>
                                    </div>
                                    <div class="flipper-back">
                                        <a href="projects.html">
                                            <div class="back-img">
                                                <img src="img/9.jpg" alt="Smiley face">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="flip-container col-xs-12 col-sm-6 col-md-4">
                                <div class="flipper">
                                    <div class="flipper-front">
                                        <div class="square">
                                            <div class="circle center-circle"></div>
                                        </div>
                                    </div>
                                    <div class="flipper-back">
                                        <a href="projects/sound.html">
                                            <div class="back-img">
                                                <img src="img/10.jpg" alt="Smiley face">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="flip-container col-xs-12 col-sm-6 col-md-4">
                                <div class="flipper">
                                    <div class="flipper-front">
                                        <div class="square">
                                            <div class="circle center-circle"></div>
                                        </div>
                                    </div>
                                    <div class="flipper-back">
                                        <a href="projects.html">
                                            <div class="back-img">
                                                <img src="img/11.jpg" alt="Smiley face">
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- END HOME GALLERY -->

            <!-- BEGIN FACTS SECTION -->
            <div class="facts-section">
                <div class="text-center">
                    <h3 class="styled-title">INTERESTING FACTS<span>...</span></h3>
                </div>

                <div id="facts" class="row clearfix">
                    <div class="col-xs-12 col-md-10 col-md-offset-1">
                        <div class="my-facts center" data-interactive>
                            <ul class="list-unstyled row">
                                <li class="progresscircles col-xs-12 col-sm-6 col-md-3">
                                    <div class="arc row-ten" data-percent="143" data-sign="" data-size="100%" data-fontSize="40" data-stokewidth="4" data-innerStrokeColor="#e4e4e4" data-circleColor="" data-textColor="#68cab6 "></div>
                                    <h4 class="fact-title">Sales</h4> <!-- Title of skill -->
                                </li>
                                <li class="progresscircles col-xs-12 col-sm-6 col-md-3">
                                    <div class="arc row-ten" data-percent="218" data-sign="" data-size="100%" data-fontSize="40" data-stokewidth="4" data-innerStrokeColor="#e4e4e4"  data-circleColor="" data-textColor="#68cab6 "></div>
                                    <h4 class="fact-title">Likes</h4> <!-- Title of skill -->
                                </li>
                                <li class="progresscircles col-xs-12 col-sm-6 col-md-3">
                                    <div class="arc row-ten" data-percent="112" data-sign="" data-size="100%" data-fontSize="40" data-stokewidth="4" data-innerStrokeColor="#e4e4e4"  data-circleColor="" data-textColor="#68cab6 "></div>
                                    <h4 class="fact-title">Clients</h4> <!-- Title of skill -->
                                </li>
                                <li class="progresscircles col-xs-12 col-sm-6 col-md-3">
                                    <div class="arc row-ten" data-percent="60" data-sign="" data-size="100%" data-fontSize="40" data-stokewidth="4" data-innerStrokeColor="#e4e4e4"  data-circleColor="" data-textColor="#68cab6 "></div>
                                    <h4 class="fact-title">Comments</h4> <!-- Title of skill -->
                                </li>
                            </ul>
                        </div>
                    </div> <!-- end div #facts -->
                </div>
            </div>
            <!-- END FACTS SECTION -->

            <div class="copyright">
                <div class="copyright-content">
                    <p> Copyright © 2017. <a href="index.php">Raj Khan</a>. All rights reserved. </p>
                </div>
            </div>


        </div>  <!-- END PAGE WRAPPER -->
    </div>  <!-- end div #page-border -->

<?php include 'inc/footer.php'; ?>   