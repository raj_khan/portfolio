
	//---------------------------------- Google map location -----------------------------------------//
	

	// Create an array of styles.
	var styles = [
					{
						stylers: [
							{ saturation: -300 }
							
						]
					},
					{
						featureType: 'road',
						elementType: 'geometry',
						stylers: [
							{ hue: "#ecf0f1" },
							{ visibility: 'simplified' }
						]
					},
					{
						featureType: 'road',
						elementType: 'labels',
						stylers: [
							{ visibility: 'off' }
						]
					}
				  ],
					
					// Lagitute and longitude for your location goes here
				   lat = 43.2166672,
				   lng = 27.9166660,
			
				  // Create a new StyledMapType object, passing it the array of styles,
				  // as well as the name to be displayed on the map type control.
				  customMap = new google.maps.StyledMapType(styles,
					  {name: 'Styled Map'}),
			
				// Create a map object, and include the MapTypeId to add
				// to the map type control.
				  mapOptions = {
					  zoom: 12,
					scrollwheel: false,
					  center: new google.maps.LatLng( lat, lng ),
					  mapTypeControlOptions: {
						  mapTypeIds: [google.maps.MapTypeId.ROADMAP],
						
					  }
				  },
				  map = new google.maps.Map(document.getElementById('map'), mapOptions),
				  myLatlng = new google.maps.LatLng( lat, lng ),

				  marker = new google.maps.Marker({
					position: myLatlng,
					map: map,
					icon: "img/marker.png"
				  });
			
				  //Associate the styled map with the MapTypeId and set it to display.
				  map.mapTypes.set('map_style', customMap);
				  map.setMapTypeId('map_style');
		
	//---------------------------------- End google map location -----------------------------------------//